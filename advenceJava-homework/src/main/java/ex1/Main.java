package ex1;

//EX1) Create 3 classes: Person, Developer, JavaDeveloper.
//        a. Person is a parent class, Developer inherits from Person, JavaDeveloper inherits from
//        Developer
//        b. Create constructor for every class that will call constructor of the super class. Each
//        constructor should display an information, that it has been called.
//        c. Create an object of type JavaDeveloper. What information will be displayed and in
//        what order?
//       ANSWER: Informatiile afisate vor fi in ordinea apelarii constructoriilor, prin urmare
//              mai intai este apelat constructorul clasei parinte pentru clasa Developer(adica clasa Person),
//              dupa constructorul cls Developer care este parintele clasei JavaDeveloper.
//              Aici avem deci "un fel de" mostenire inlantuita wanna be asta pentru ca
//              in java doar o clasa poate extinde cel mult o alta clasa. (dar poate implementa oricate interfete ;) )
//        d. Using an object of type JavaDeveloper call a method that is defined in Developer class.
//          What access modifier should it have?
//       ANSWER: PROTECTED deoarece este singurul modificator de acces care permite accestul dintr-o
//                  clasa care extinde clasa de baza. (clasa care extinde clasa de baza s.n. clasa derivata)
//        e. *Overload method from the Person class in JavaDeveloper class to accept additional
//        parameters.
//          Aici sincer nu inteleg ce trebuie sa fac. Nu sunt multumita de metoda aia din JavaDeveloper,
// sigur are nevoie de improvements.


public class Main {

    public static void main(String[] args) {

        JavaDeveloper javaDeveloper = new JavaDeveloper("Livia", 25, "Sibiu",
                                                         "javaDeveloper wanna be", 5000);
        javaDeveloper.displayDetails(5000);
    }
}
