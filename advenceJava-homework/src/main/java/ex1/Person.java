package ex1;

public class Person {
    private String name;
    private int age;
    private String address;

    //this fulfills requirement b.
    public Person(String name, int age, String address) {
        this.name = name;
        this.age = age;
        this.address = address;
        System.out.println("Person constructor has been called.");
    }

    public void displayDetails() {
        System.out.println(name + " is " + age + " and she lives in " + address + " ");
    }
}
