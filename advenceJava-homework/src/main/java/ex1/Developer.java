package ex1;

public class Developer extends Person {
    private String occupation;

    //this fulfills requirement b.
    public Developer(String name, int age, String address, String occupation) {
        super(name, age, address);
        this.occupation = occupation;
        System.out.println("Developer constructor has been called.");
    }

    //this fulfills a part of requirement d.
    protected void printOccupation() {
        System.out.println("I am a " + occupation);
    }
}

