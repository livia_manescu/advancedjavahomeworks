package ex1;

public class JavaDeveloper extends Developer {

    private double salary;

    //this fulfills requirement b.
    public JavaDeveloper(String name, int age, String address, String occupation, double salary) {
        super(name, age, address, occupation);
        this.salary = salary;
        System.out.println("JavaDeveloper constructor has been called.");
    }

    //this fulfills requirement e.
    public void displayDetails(double salary) {
        super.displayDetails();
        //and displaying the parameter tho
        System.out.println(salary);
    }

}
